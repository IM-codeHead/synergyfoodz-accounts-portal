import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { HomePage, RawMaterials, Suppliers, PurchaseBills, Payments } from './routes';
import './App.scss';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route
              exact
              key={'/'}
              path={'/'}
              component={HomePage}
            />
            <Route
              exact
              key={'app/rawMaterials'}
              path={'/rawMaterials'}
              component={RawMaterials}
            />
            <Route
              exact
              key={'app/suppliers'}
              path={'/suppliers'}
              component={Suppliers}
            />
            <Route
              exact
              key={'app/purchaseBills'}
              path={'/purchaseBills'}
              component={PurchaseBills}
            />
            <Route
              exact
              key={'app/payments'}
              path={'/payments'}
              component={Payments}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
