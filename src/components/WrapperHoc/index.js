import React from 'react';
import Menu from '../Menu';

const WrapperHoc = WrappedComponent => class extends React.PureComponent {
  render() {
    return (
      <div className="page-holder">
        <Menu history={this.props.history} />
        <div className="page">
          <WrappedComponent {...this.props} />
        </div>
      </div>
    );
  }
};

export default WrapperHoc;
