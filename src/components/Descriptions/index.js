import React, {Component} from 'react';
import { Descriptions } from 'antd';
import './index.scss';

export default class DescriptionsTable extends Component {
  renderDescItems() {
    return (
      this.props.items.map(item => {
        return (
          <Descriptions.Item label={item.label} >{item.value}</Descriptions.Item>
        );
      })
    );
  }

  render() {
    return (
      <div className="descriptions-container" style={{ width: this.props.width }}>
        <Descriptions bordered {...this.props}>
          {this.renderDescItems()}
        </Descriptions>
      </div>
    );
  }
}
