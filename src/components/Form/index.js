import React, {Component} from 'react';
import { Button, DatePicker, Form, Input, InputNumber, Select, Radio } from 'antd';

import './index.scss';

const FormItem = Form.Item;
const { Option } = Select;

class EditForm extends Component {
  onSubmit = () => {
    this.props.onSubmit(this.props.form);
  }

  onExtraClick = () => {
    this.props.onExtraClick(this.props.form);
  }

  renderInputItem(formItem) {
    const { type, inputProps = {}, placeholder } = formItem;
    switch (type) {
      case 'text':
        return (<Input
          placeholder={placeholder} style={{ width: inputProps.width }}
        />);
      case 'number':
        return (<InputNumber
          placeholder={placeholder} style={{ width: inputProps.width }}
        />);
      case 'textArea':
        return (<Input.TextArea rows={inputProps.rows} style={{ width: inputProps.width }}
          placeholder={placeholder}
        />);
      case 'select':
        return (
          <Select size={inputProps.size || 'default'} style={{ width: inputProps.width }}>
            {inputProps.options.map(option => <Option key={option.value} value={option.value}>{option.label}</Option>)}
          </Select>
        );
      case 'date':
        return (<DatePicker format="DD/MM/YYYY" />);
      case 'radio':
        return (
          <Radio.Group onChange={inputProps.onChange} >
            {inputProps.options.map(option => <Radio key={option.value}  value={option.value}>{option.label}</Radio>)}
          </Radio.Group>
        );
      default:
        break;
    }
  }

  renderFormItems() {
    const { getFieldDecorator } = this.props.form;
    const { formItems } = this.props;
    return formItems.map(formItem => {
      const { label = '', key, isRequired = false, validations = [], value } = formItem;
      return (
        <FormItem label={label}>
          {
            getFieldDecorator(key, {
              initialValue: value,
              rules: [
                {
                  required: isRequired,
                  message: `${label} is required`,
                },
                ...validations,
              ]
            })(this.renderInputItem(formItem))
          }
        </FormItem>
      );
    });
  }

  render() {
    return (
      <div className={`custom-form`}>
        <Form layout="horizontal">
          {this.renderFormItems()}
        </Form>
        <div className="flex-center-center">
          <Button
            type="primary"
            onClick={this.onSubmit}
            style={{ marginRight: 20 }}
          >
            {this.props.submitBtnText || 'Submit'}
          </Button>
          {this.props.showExtraBtn && (
            <Button
              type="primary"
              onClick={this.onExtraClick}
              style={{ marginRight: 20 }}
            >
              {this.props.extraBtnText}
            </Button>
          )}
        </div>
      </div>
    );
  }
}

export default Form.create()(EditForm);
