import React, {Component} from 'react';
import { Menu, Icon, Button } from 'antd';
import './index.scss';

const routes = [
  { key: 'dashboard', path: '/', name: 'Dashboard', icon: 'dashboard' },
  { key: 'rawMaterials', path: '/rawMaterials', name: 'Raw Materials', icon: 'build' },
  { key: 'suppliers', path: '/suppliers', name: 'Suppliers', icon: 'pie-chart' },
  { key: 'purchaseBills', path: '/purchaseBills', name: 'Purchase Bills', icon: 'container' },
  { key: 'payments', path: '/payments', name: 'Payments', icon: 'credit-card' },
  { key: 'reports', path: '/reports', name: 'MIS Reports', icon: 'database' },
  { key: 'changePassword', path: '/changePassword', name: 'Change Password', icon: 'edit' },
  { key: 'logout', path: '/logout', name: 'Logout', icon: 'logout' },
];

export default class Sider extends Component {
  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  handleClick = e => {
    const history = this.props.history;
    history.push(routes.find(route => route.key === e.key).path);
  };

  render() {
    return (
      <div className="cs-menu-slider">
        <div className={`slider-btn-holder ${this.state.collapsed ? 'collapsed' : 'expand'}`}>
          <Button type="primary" onClick={this.toggleCollapsed}>
            <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
          </Button>
        </div>
        <Menu
          onClick={this.handleClick}
          mode="inline"
          theme="dark"
          inlineCollapsed={this.state.collapsed}
        >
          {routes.map(route => <Menu.Item key={route.key}>
            <Icon type={route.icon} />
            <span>{route.name}</span>
          </Menu.Item>)}
        </Menu>
      </div>
    );
  }
}
