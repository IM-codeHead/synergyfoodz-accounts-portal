import React, {Component} from 'react';
import { Collapse, Icon } from 'antd';
import Form from '../Form';

const { Panel } = Collapse;

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden',
};

export default class SearchBox extends Component {
  handleSearch = form => {
    form.validateFields((err, values) => {
      this.props.onSearch(values);
    });
  };

  handleReset = form => {
    form.resetFields();
  };

  render() {
    return (
    <Collapse
      bordered={false}
      defaultActiveKey={['1']}
      expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
    >
    <Panel header="Search" key="1" style={customPanelStyle}>
      <Form
        showExtraBtn
        submitBtnText="Search"
        onSubmit={this.handleSearch}
        formItems={this.props.formItems}
        onExtraClick={this.handleReset}
        extraBtnText="Reset"
      />
    </Panel>
  </Collapse>);
  }
}
