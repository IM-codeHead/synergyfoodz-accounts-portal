import HomePage from './HomePage';
import RawMaterials from './RawMaterials';
import Suppliers from './Suppliers';
import PurchaseBills from './PurchaseBills';
import Payments from './Payments';

export {
  HomePage,
  RawMaterials,
  Suppliers,
  PurchaseBills,
  Payments,
}
