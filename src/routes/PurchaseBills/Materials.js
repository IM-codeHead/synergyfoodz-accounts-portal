import React, {Component} from 'react';
import { Button } from 'antd';
import MaterialsList from './MaterialsList';
import MaterialForm from './MaterialForm';

const EDIT = 'edit';
const NEW = 'new';

export default class Materials extends Component {
  state = { showDrawer: false, mode: null, materials: [], currentMaterial: null };

  componentDidMount() {
    this.setState({ materials: this.props.materials });
  }

  onNew = () => {
    this.setState({ showDrawer: true, mode: NEW });
  }

  onEdit = (itemName) => {
    const currentMaterial = this.state.materials.find(el => el.itemName === itemName);
    this.setState({ showDrawer: true, mode: EDIT, currentMaterial });
  }

  hideDrawer = () => {
    this.setState({ showDrawer: false, mode: null });
  }

  onSubmit = (form) => {
    const {validateFields, getFieldsValue} = form;
    validateFields((errors) => {
      if (errors) {
        return errors;
      }
      const fieldValues = getFieldsValue();
      if (this.state.mode === EDIT) {
        this.onEditSubmit(fieldValues);
      } else if (this.state.mode === NEW) {
        this.onAddNewSubmit(fieldValues);
      }
    });
  }

  onEditSubmit = (values) => {
    const { materials, currentMaterial } = this.state;
    const currentMaterialIndex = materials.findIndex(el => el.itemName === currentMaterial.itemName);
    materials[currentMaterialIndex] = { ...values };
    this.props.updateMaterials(materials);
    this.setState({ currentMaterial: null });
    this.hideDrawer();
  }

  onAddNewSubmit = (values) => {
    const { materials } = this.state;
    materials.push({ ...values });
    this.props.updateMaterials(materials);
    this.hideDrawer();
  }

  render() {
    const { mode } = this.state;
    return (
      <>
        <Button type="primary" onClick={this.onNew}>Add new material</Button>
        <MaterialsList
          materials={this.state.materials}
          onEdit={this.onEdit}
        />
        {this.state.showDrawer && <MaterialForm
          title={(mode === EDIT) ? 'Edit material' : 'New material'}
          onClose={this.hideDrawer}
          visible={this.state.showDrawer}
          submitBtnText={(mode === EDIT) ? 'Save' : 'Save and Add'}
          onSubmit={this.onSubmit}
          initialValues={this.state.currentMaterial || {}}
          rawMaterials={this.props.rawMaterials}
        />}
      </>
    );
  }
}
