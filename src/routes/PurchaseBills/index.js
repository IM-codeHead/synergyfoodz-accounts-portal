import React, {Component} from 'react';
import WrapperHoc from '../../components/WrapperHoc';
import { Button, Select } from 'antd';
import BillsList from './BillsList';
import BillForm from './BillForm';
import { database } from '../../firebase';

import './index.scss';

const EDIT = 'edit';
const NEW = 'new';

const { Option } = Select;

class PurchaseBills extends Component {
  state = { showDrawer: false, mode: null, locations: [], currentLocation: null, purchaseBills: [], currentBill: null, rawMaterials: [] };
  purchaseBills = [];

  componentDidMount() {
    database.ref('/locations').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ locations: state.map(el => el.locationId) });
    });
    database.ref('/rawMaterials').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ rawMaterials: state });
    });
  }

  updateBillsInDB(purchaseBills) {
    const rootRef = database.ref();
    const storesRef = rootRef.child(`/purchaseBills/${this.state.currentLocation}`);
    storesRef.set(purchaseBills);
  }

  updateMaterialsInBill = (materials) => {
    const { purchaseBills, currentBill } = this.state;
    const currentBillIndex = purchaseBills.findIndex(el => el.billNumber === currentBill.billNumber);
    purchaseBills[currentBillIndex].materials = { ...materials };
    const { totalCGST = 0, totalSGST = 0, totalIGST = 0, totalTaxable = 0, totalDiscount = 0, grossBillAmount = 0 } = this.calculateAmounts(purchaseBills[currentBillIndex].materials);
    purchaseBills[currentBillIndex] = {
      ...purchaseBills[currentBillIndex],
      // totalCGST,
      // totalSGST,
      // totalIGST,
      totalTaxable,
      totalDiscount,
      grossBillAmount,
    };
    this.updateBillsInDB(purchaseBills);
  }

  onNew = () => {
    this.setState({ showDrawer: true, mode: NEW });
  }

  onEdit = (billNumber) => {
    const currentBill = this.state.purchaseBills.find(el => el.billNumber === billNumber);
    this.setState({ showDrawer: true, mode: EDIT, currentBill });
  }

  hideDrawer = () => {
    this.setState({ showDrawer: false, mode: null });
  }

  onSubmit = (form) => {
    const {validateFields, getFieldsValue} = form;
    validateFields((errors) => {
      if (errors) {
        return errors;
      }
      const fieldValues = getFieldsValue();
      const dateKeys = ['billDate', 'challanDate', 'orderDate', 'receiptDate'];
      Object.keys(fieldValues).forEach(key => {
        if (dateKeys.includes(key)) {
          fieldValues[key] = fieldValues[key].format('DD/MM/YYYY');
        }
      });
      if (this.state.mode === EDIT) {
        this.onEditSubmit(fieldValues);
      } else if (this.state.mode === NEW) {
        this.onAddNewSubmit(fieldValues);
      }
    });
  }

  calculateAmounts(materials) {
    if (materials && materials.length) {
      const sum = (acc, current) => (acc + current);
      materials = materials.map(el => { return { ...el, taxableAmount: (el.quantity * el.purchaseRate) }});
      const totalCGST = materials.map(el => (el.CGST * el.taxableAmount)).reduce(sum);
      const totalSGST = materials.map(el => (el.SGST * el.taxableAmount)).reduce(sum);
      const totalIGST = materials.map(el => (el.IGST * el.taxableAmount)).reduce(sum);
      const totalTaxable = materials.map(el => (el.taxableAmount)).reduce(sum);
      const totalDiscount = materials.map(el => (el.discountAmount)).reduce(sum);
      const grossBillAmount = (totalTaxable + totalCGST + totalSGST + totalIGST) - totalDiscount;
      return { totalCGST, totalSGST, totalIGST, totalTaxable, totalDiscount, grossBillAmount };
    }
    return {};
  }

  onEditSubmit = (values) => {
    const { purchaseBills, currentBill } = this.state;
    const currentBillIndex = purchaseBills.findIndex(el => el.billNumber === currentBill.billNumber);
    purchaseBills[currentBillIndex] = { ...purchaseBills[currentBillIndex], ...values };
    const { totalCGST = 0, totalSGST = 0, totalIGST = 0, totalTaxable = 0, totalDiscount = 0, grossBillAmount = 0 } = this.calculateAmounts(purchaseBills[currentBillIndex].materials);
    purchaseBills[currentBillIndex] = {
      ...purchaseBills[currentBillIndex],
      // totalCGST,
      // totalSGST,
      // totalIGST,
      totalTaxable,
      totalDiscount,
      grossBillAmount,
    };
    this.updateBillsInDB(purchaseBills);
    this.setState({ currentBill: null });
    this.hideDrawer();
  }

  onAddNewSubmit = (values) => {
    const { purchaseBills } = this.state;
    purchaseBills.push({ ...values });
    this.updateBillsInDB(purchaseBills);
    this.hideDrawer();
  }

  onLocationChange = value => {
    this.setState({ currentLocation: value }, () => {
      database.ref(`/purchaseBills/${this.state.currentLocation}`).on('value', snapshot => {
        const state = snapshot.val();
        this.setState({ purchaseBills: state });
      });
    });
  }

  render() {
    const { mode } = this.state;

    return (
      <div>
        <div className="select-location">
          <div style={{ marginRight: 10 }}><strong>Select Location </strong></div>
          <Select style={{ width: 120 }} onChange={this.onLocationChange}>
            {this.state.locations.map(locationId => <Option key={locationId} value={locationId}>{locationId}</Option>)}
          </Select>
        </div>
        {this.state.currentLocation && (
          <div className="location-bill-card flex-center-center flex-column">
            <div className="title">{this.state.currentLocation}</div>
            <Button type="primary" onClick={this.onNew}>Add new bill</Button>
            <BillsList
              purchaseBills={this.state.purchaseBills}
              onEdit={this.onEdit}
            />
            {this.state.showDrawer && <BillForm
              title={(mode === EDIT) ? 'Edit bill' : 'New bill'}
              onClose={this.hideDrawer}
              visible={this.state.showDrawer}
              submitBtnText={(mode === EDIT) ? 'Save' : 'Save and Add'}
              onSubmit={this.onSubmit}
              initialValues={this.state.currentBill || {}}
              rawMaterials={this.state.rawMaterials}
              updateMaterials={this.updateMaterialsInBill}
            />}
          </div>
        )}
        {!this.state.currentLocation && <div>Please select location from the above list</div>}
      </div>
    );
  }
}

export default WrapperHoc(PurchaseBills);
