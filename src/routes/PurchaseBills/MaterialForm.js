import React, {Component} from 'react';
import { Drawer } from 'antd';
import Form from '../../components/Form';
// import { database } from '../../firebase';

export default class MaterialForm extends Component {
  render() {
    const { initialValues } = this.props;
    const materials = this.props.rawMaterials.map(material => { return { value: material.itemName, label: material.itemName } });
    const formItems = [
      {
        label: 'Item Name',
        key: 'itemName',
        isRequired: true,
        inputProps: {
          width: 320,
          options: materials,
        },
        type: 'select',
        value: initialValues.itemName,
      },
      {
        label: 'Quantity',
        key: 'quantity',
        isRequired: true,
        placeholder: 'Quantity',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.quantity,
      },
      {
        label: 'Rate',
        key: 'purchaseRate',
        isRequired: true,
        placeholder: 'Rate',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.purchaseRate,
      },
      {
        label: 'Discount Amount',
        key: 'discountAmount',
        isRequired: false,
        placeholder: 'Discount Amount',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.discountAmount,
      },
      {
        label: 'Discount Rate',
        key: 'discountRate',
        isRequired: false,
        placeholder: 'Taxable Rate',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.discountRate,
      },
      {
        label: 'SGST%',
        key: 'SGST',
        isRequired: false,
        placeholder: 'SGST%',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.SGST,
      },
      {
        label: 'CGST%',
        key: 'CGST',
        isRequired: false,
        placeholder: 'CGST%',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.CGST,
      },
      {
        label: 'IGST%',
        key: 'IGST',
        isRequired: false,
        placeholder: 'IGST%',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
          }
        ],
        value: initialValues.IGST,
      },
    ];

    return (
      <Drawer
        title={this.props.drawerTitle}
        width={'80%'}
        onClose={this.props.onClose}
        visible={this.props.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          submitBtnText={this.props.submitBtnText}
          onSubmit={this.props.onSubmit}
          formItems={formItems}
        />
      </Drawer>
    );
  }
}
