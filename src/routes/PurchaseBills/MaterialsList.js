import React, {Component} from 'react';
import { Table, Button } from 'antd';

export default class MaterialsList extends Component {
  render() {
    const materials = this.props.materials.map(item => {
      const { quantity = 0, purchaseRate = 0, CGST = 0, SGST = 0, IGST = 0, discountRate = 0, discountAmount = 0 } = item;
      const taxableAmount = quantity * purchaseRate;
      const cgstAmount = taxableAmount * (CGST / 100);
      const sgstAmount = taxableAmount * (SGST / 100);
      const igstAmount = taxableAmount * (IGST / 100);
      const netAmount = (taxableAmount + cgstAmount + sgstAmount + igstAmount) - (discountAmount ? discountAmount : (taxableAmount * (discountRate / 100 )));
      return {
        ...item,
        taxableAmount,
        cgstAmount,
        sgstAmount,
        igstAmount,
        discountAmount: discountAmount ? discountAmount : (taxableAmount * (discountRate / 100 )),
        discountRate: discountRate ? discountRate : ((discountAmount / taxableAmount) * 100),
        netAmount,
      }
    });
    const columns = [
      {
        title: 'Action',
        dataIndex: 'itemName',
        key: 'editButton',
        render: (itemName) => <Button icon="edit" onClick={() => this.props.onEdit(itemName)} />
      },
      {
        title: 'Item Name',
        dataIndex: 'itemName',
        key: 'itemName',
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity',
      },
      {
        title: 'Rate',
        dataIndex: 'purchaseRate',
        key: 'purchaseRate',
      },
      {
        title: 'Taxable Amount',
        dataIndex: 'taxableAmount',
        key: 'taxableAmount',
      },
      {
        title: 'SGST%',
        dataIndex: 'SGST',
        key: 'SGST',
      },
      {
        title: 'SGST Amount',
        dataIndex: 'sgstAmount',
        key: 'sgstAmount',
      },
      {
        title: 'CGST%',
        dataIndex: 'CGST',
        key: 'CGST',
      },
      {
        title: 'CGST Amount',
        dataIndex: 'cgstAmount',
        key: 'cgstAmount',
      },
      {
        title: 'IGST%',
        dataIndex: 'IGST',
        key: 'IGST',
      },
      {
        title: 'IGST Amount',
        dataIndex: 'igstAmount',
        key: 'igstAmount',
      },
      {
        title: 'Discount Rate',
        dataIndex: 'discountRate',
        key: 'discountRate',
      },
      {
        title: 'Discount Amount',
        dataIndex: 'discountAmount',
        key: 'discountAmount',
      },
      {
        title: 'Net Amount',
        dataIndex: 'netAmount',
        key: 'netAmount',
      },
    ];

    return (
      <div className="list materials-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
          dataSource={materials}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['itemName']}
        />
      </div>
    );
  }
}
