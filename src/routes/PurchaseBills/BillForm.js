import React, {Component} from 'react';
import { Drawer } from 'antd';
import Form from '../../components/Form';
import Descriptions from '../../components/Descriptions';
import moment from 'moment';
import { database } from '../../firebase';
import Materials from './Materials';

export default class BillForm extends Component {
  state = { suppliers: [] };

  componentDidMount() {
    database.ref('/suppliers').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ suppliers: state.map(el => el.supplierName) });
    });
  }

  calculateAmounts() {
    let { initialValues: { materials } } = this.props;
    if (materials && materials.length) {
      const sum = (acc, current) => (acc + current);
      materials = materials.map(el => { return { ...el, taxableAmount: (el.quantity * el.purchaseRate) }});
      const totalCGST = materials.map(el => (el.CGST * el.taxableAmount)).reduce(sum);
      const totalSGST = materials.map(el => (el.SGST * el.taxableAmount)).reduce(sum);
      const totalIGST = materials.map(el => (el.IGST * el.taxableAmount)).reduce(sum);
      const totalTaxable = materials.map(el => (el.taxableAmount)).reduce(sum);
      const totalDiscount = materials.map(el => (el.discountAmount)).reduce(sum);
      const grossBillAmount = (totalTaxable + totalCGST + totalSGST + totalIGST) - totalDiscount;
      return { totalCGST, totalSGST, totalIGST, totalTaxable, totalDiscount, grossBillAmount };
    }
    return {};
  }

  render() {
    const { initialValues } = this.props;
    const suppliers = this.state.suppliers.map(supplier => { return { value: supplier, label: supplier }; });
    const { totalCGST = 0, totalSGST = 0, totalIGST = 0, totalTaxable = 0, totalDiscount = 0, grossBillAmount = 0 } = this.calculateAmounts();
    const formItems = [
      {
        label: 'Bill No.',
        key: 'billNumber',
        isRequired: true,
        placeholder: 'Purchase Bill No.',
        type: 'text',
        inputProps: {
          width: 350,
        },
        value: initialValues.billNumber,
      },
      {
        label: 'Bill Date',
        key: 'billDate',
        isRequired: true,
        placeholder: '',
        type: 'date',
        value: initialValues.billDate ? moment(initialValues.billDate, 'DD/MM/YYYY') : null,
      },
      {
        label: 'Delivery Challan No.',
        key: 'challanNumber',
        isRequired: true,
        placeholder: 'Delivery Challan No.',
        type: 'text',
        inputProps: {
          width: 350,
        },
        value: initialValues.challanNumber,
      },
      {
        label: 'Delivery Challan Date',
        key: 'challanDate',
        isRequired: true,
        placeholder: '',
        type: 'date',
        value: initialValues.challanDate ? moment(initialValues.challanDate, 'DD/MM/YYYY') : null,
      },
      {
        label: 'Supplier Name',
        key: 'supplierName',
        isRequired: true,
        inputProps: {
          width: 320,
          options: suppliers,
        },
        type: 'select',
        value: initialValues.supplierName,
      },
      {
        label: 'Purchase Order No.',
        key: 'orderNumber',
        isRequired: true,
        placeholder: 'Purchase Order No.',
        type: 'text',
        inputProps: {
          width: 350,
        },
        value: initialValues.orderNumber,
      },
      {
        label: 'Purchase Order Date',
        key: 'orderDate',
        isRequired: true,
        placeholder: '',
        type: 'date',
        value: initialValues.orderDate ? moment(initialValues.orderDate, 'DD/MM/YYYY') : null,
      },
      {
        label: 'Goods Receipt No.',
        key: 'receiptNumber',
        isRequired: true,
        placeholder: 'Goods Receipt No.',
        type: 'text',
        inputProps: {
          width: 350,
        },
        value: initialValues.receiptNumber,
      },
      {
        label: 'Goods Receipt Date',
        key: 'receiptDate',
        isRequired: true,
        placeholder: '',
        type: 'date',
        value: initialValues.receiptDate ? moment(initialValues.receiptDate, 'DD/MM/YYYY') : null,
      },
    ];

    const amountTableItems = [
      {
        label: "Total Taxable Amount",
        value: totalTaxable,
      },
      {
        label: "Total Discount Amount",
        value: totalDiscount,
      },
      {
        label: "Total CGST",
        value: totalCGST,
      },
      {
        label: "Total SGST",
        value: totalSGST,
      },
      {
        label: "Total IGST",
        value: totalIGST,
      },
      {
        label: "Gross Bill Amount",
        value: grossBillAmount,
      },
    ];

    return (
      <Drawer
        title={this.props.drawerTitle}
        width={'80%'}
        onClose={this.props.onClose}
        visible={this.props.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          submitBtnText={this.props.submitBtnText}
          onSubmit={this.props.onSubmit}
          formItems={formItems}
        />
        <Materials
          materials={initialValues.materials || []}
          rawMaterials={this.props.rawMaterials}
          updateMaterials={this.props.updateMaterials}
        />
        <Descriptions
          size="small"
          layout="horizontal"
          column={1}
          items={amountTableItems}
          width={'22rem'}
        />
      </Drawer>
    );
  }
}
