import React, {Component} from 'react';
import { Table, Button } from 'antd';

export default class BillsList extends Component {
  render() {
    const columns = [
      {
        title: 'Action',
        dataIndex: 'billNumber',
        key: 'editButton',
        render: (billNumber) => <Button icon="edit" onClick={() => this.props.onEdit(billNumber)} />
      },
      {
        title: 'GR',
        dataIndex: 'receiptNumber',
        key: 'receiptNumber',
      },
      {
        title: 'GR Date',
        dataIndex: 'receiptDate',
        key: 'receiptDate',
      },
      {
        title: 'Bill No.',
        dataIndex: 'billNumber',
        key: 'billNumber',
      },
      {
        title: 'Supplier Name',
        dataIndex: 'supplierName',
        key: 'supplierName',
      },
      // {
      //   title: 'Bill amount',
      //   dataIndex: 'grossAmount',
      //   key: 'grossAmount',
      // },
    ];

    return (
      <div className="list bills-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
            dataSource={this.props.purchaseBills}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['billNumber']}
        />
      </div>
    );
  }
}
