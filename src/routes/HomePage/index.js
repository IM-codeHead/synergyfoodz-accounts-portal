import React, {Component} from 'react';
import WrapperHoc from '../../components/WrapperHoc';

class HomePage extends Component {
  render() {
    return (
      <div>
        Welcome to admin portal.
      </div>
    );
  }
}

export default WrapperHoc(HomePage);
