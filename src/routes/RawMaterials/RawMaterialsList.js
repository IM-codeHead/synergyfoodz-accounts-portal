import React, {Component} from 'react';
import { Table, Button } from 'antd';

export default class RawMaterialsList extends Component {
  render() {
    const columns = [
      {
        title: 'Action',
        dataIndex: 'itemName',
        key: 'editButton',
        render: (itemName) => <Button icon="edit" onClick={() => this.props.onEdit(itemName)} />
      },
      {
        title: 'Item Name',
        dataIndex: 'itemName',
        key: 'itemName',
      },
      {
        title: 'Item Description',
        dataIndex: 'itemDescription',
        key: 'itemDescription',
      },
      {
        title: 'GST%',
        dataIndex: 'GST',
        key: 'GST',
      },
      {
        title: 'Purchase Rate',
        dataIndex: 'purchaseRate',
        key: 'purchaseRate',
      },
      {
        title: 'Unit',
        dataIndex: 'UOM',
        key: 'UOM',
      },
      {
        title: 'Opening Qty',
        dataIndex: 'openingQuantity',
        key: 'openingQuantity',
      },
    ];

    return (
      <div className="list materials-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
          dataSource={this.props.rawMaterials}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['itemName']}
        />
      </div>
    );
  }
}
