import React, {Component} from 'react';
import { Drawer } from 'antd';
import Form from '../../components/Form';

const UNITS = [
  { value: 'Numbers', label: 'nos' },
  { value: 'Kilogram', label: 'kg' },
  { value: 'Gram', label: 'g' },
  { value: 'Litre', label: 'l' },
  { value: 'Mililitre', label: 'ml' },
  { value: 'Meter', label: 'm' },
  { value: 'Centimeter', label: 'cm' },
  { value: 'Milimeter', label: 'mm' },
  { value: 'Kilometer', label: 'km' },
];

export default class RawMaterialsForm extends Component {
  render() {
    const { initialValues } = this.props;
    const formItems = [
      {
        label: 'Item Name',
        key: 'itemName',
        isRequired: true,
        placeholder: 'Enter Item Name',
        type: 'text',
        inputProps: {
          width: 350,
        },
        value: initialValues.itemName,
      },
      {
        label: 'Item Description',
        key: 'itemDescription',
        isRequired: false,
        placeholder: 'Enter Description',
        type: 'textArea',
        inputProps: { rows: 2, width: 350 },
        value: initialValues.itemDescription,
      },
      {
        label: 'GST%',
        key: 'GST',
        isRequired: true,
        placeholder: 'GST%',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
            max: 28,
            message: 'GST% must be between 0 and 28',
          }
        ],
        value: initialValues.GST,
      },
      {
        label: 'Purchase Rate',
        key: 'purchaseRate',
        isRequired: true,
        placeholder: 'Rate',
        type: 'number',
        validations: [
          {
            type: 'number',
            min: 0,
            message: 'Purchase Rate must be equal or greater than 0',
          },
        ],
        inputProps: {
          width: 150,
        },
        value: initialValues.purchaseRate,
      },
      {
        label: 'UOM',
        key: 'UOM',
        isRequired: true,
        inputProps: {
          width: 120,
          options: UNITS,
        },
        type: 'select',
        value: initialValues.UOM,
      },
      {
        label: 'Opening Qty',
        key: 'openingQuantity',
        isRequired: true,
        placeholder: 'Opening Qty',
        validations: [
          {
            type: 'number',
            min: 0,
            message: 'Opening Qty must be equal or greater than 0',
          },
        ],
        inputProps: {
          width: 250,
        },
        type: 'number',
        value: initialValues.openingQuantity,
      }
    ];

    return (
      <Drawer
        title={this.props.drawerTitle}
        width={'100%'}
        onClose={this.props.onClose}
        visible={this.props.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          submitBtnText={this.props.submitBtnText}
          onSubmit={this.props.onSubmit}
          formItems={formItems}
        />
      </Drawer>
    );
  }
}
