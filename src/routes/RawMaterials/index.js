import React, {Component} from 'react';
import WrapperHoc from '../../components/WrapperHoc';
import { Button } from 'antd';
import RawMaterialsForm from './RawMaterialsForm';
import RawMaterialsList from './RawMaterialsList';
import { database } from '../../firebase';
import './index.scss';

const EDIT = 'edit';
const NEW = 'new';

class RawMaterials extends Component {
  state = { showDrawer: false, mode: null, rawMaterials: [], currentMaterial: null };
  rawMaterials = [];

  componentDidMount() {
    database.ref('/rawMaterials').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ rawMaterials: state });
      this.rawMaterials = state;
    });
  }

  updateDB(rawMaterials) {
    const rootRef = database.ref();
    const storesRef = rootRef.child('/rawMaterials');
    storesRef.set(rawMaterials);
  }

  onNew = () => {
    this.setState({ showDrawer: true, mode: NEW });
  }

  onEdit = (itemName) => {
    const currentMaterial = this.state.rawMaterials.find(el => el.itemName === itemName);
    this.setState({ showDrawer: true, mode: EDIT, currentMaterial });
  }

  hideDrawer = () => {
    this.setState({ showDrawer: false, mode: null });
  }

  onSubmit = (form) => {
    const {validateFields, getFieldsValue} = form;
    validateFields((errors) => {
      if (errors) {
        return errors;
      }
      const fieldValues = getFieldsValue();
      if (this.state.mode === EDIT) {
        this.onEditSubmit(fieldValues);
      } else if (this.state.mode === NEW) {
        this.onAddNewSubmit(fieldValues);
      }
    });
  }

  onEditSubmit = (values) => {
    const { rawMaterials, currentMaterial } = this.state;
    const currentMaterialIndex = rawMaterials.findIndex(el => el.itemName === currentMaterial.itemName);
    rawMaterials[currentMaterialIndex] = { ...values };
    this.updateDB(rawMaterials);
    this.setState({ currentMaterial: null });
    this.hideDrawer();
  }

  onAddNewSubmit = (values) => {
    const { rawMaterials } = this.state;
    rawMaterials.push({ ...values });
    this.updateDB(rawMaterials);
    this.hideDrawer();
  }

  render() {
    const { mode } = this.state;
    return (
      <div>
        <Button type="primary" onClick={this.onNew}>Add new material</Button>
        {this.state.showDrawer && <RawMaterialsForm
          title={(mode === EDIT) ? 'Edit material' : 'New material'}
          onClose={this.hideDrawer}
          visible={this.state.showDrawer}
          submitBtnText={(mode === EDIT) ? 'Save' : 'Save and Add'}
          onSubmit={this.onSubmit}
          initialValues={this.state.currentMaterial || {}}
        />}
        <RawMaterialsList
          rawMaterials={this.state.rawMaterials}
          onEdit={this.onEdit}
        />
      </div>
    );
  }
}

export default WrapperHoc(RawMaterials);
