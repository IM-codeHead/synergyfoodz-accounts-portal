import React, {Component} from 'react';
import { Button, Row, Form, Input } from 'antd'

import './index.scss';

const FormItem = Form.Item;

class LoginPage extends Component {

  handleOk = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.history.push('/');
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-page">
        <div>
          Login
        </div>
        <Form>
          <FormItem hasFeedback>
            {getFieldDecorator('username', {
              rules: [
                {
                  required: true,
                  message: 'Please input username'
                }
              ]
            })(<Input size='large' placeholder='username' />)}
          </FormItem>
          <FormItem hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please input password'
                }
              ]
            })(<Input size='large' type='password' placeholder='password' />)}
          </FormItem>
          <Row>
            <Button type='submit' size='large' onClick={this.handleOk}>
              Login
            </Button>
            &nbsp;
          </Row>

        </Form>
      </div>
    );
  }
}

const WrappedLoginForm = Form.create({ name: 'login' })(LoginPage);

export default WrappedLoginForm;
