import React, {Component} from 'react';
import WrapperHoc from '../../components/WrapperHoc';
import { Button } from 'antd';
import SupplierForm from './SupplierForm';
import SearchSupplier from './SearchSuppliers';
import SuppliersList from './SuppliersList';
import { database } from '../../firebase';
import './index.scss';

const EDIT = 'edit';
const NEW = 'new';

class Suppliers extends Component {
  state = { showDrawer: false, mode: null, suppliers: [], currentSupplier: null };
  suppliers = [];

  componentDidMount() {
    database.ref('/suppliers').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ suppliers: state });
      this.suppliers = state;
    });
  }

  updateDB(suppliers) {
    const rootRef = database.ref();
    const storesRef = rootRef.child('/suppliers');
    storesRef.set(suppliers);
  }

  onNew = () => {
    this.setState({ showDrawer: true, mode: NEW });
  }

  onEdit = (supplierName) => {
    const currentSupplier = this.state.suppliers.find(el => el.supplierName === supplierName);
    this.setState({ showDrawer: true, mode: EDIT, currentSupplier });
  }

  hideDrawer = () => {
    this.setState({ showDrawer: false, mode: null });
  }

  onSubmit = (form) => {
    const {validateFields, getFieldsValue} = form;
    validateFields((errors) => {
      if (errors) {
        return errors;
      }
      const fieldValues = getFieldsValue();
      if (this.state.mode === EDIT) {
        this.onEditSubmit(fieldValues);
      } else if (this.state.mode === NEW) {
        this.onAddNewSubmit(fieldValues);
      }
    });
  }

  onEditSubmit = (values) => {
    const { suppliers, currentSupplier } = this.state;
    const currentSupplierIndex = suppliers.findIndex(el => el.supplierName === currentSupplier.supplierName);
    suppliers[currentSupplierIndex] = { ...values };
    this.updateDB(suppliers);
    this.setState({ currentSupplier: null });
    this.hideDrawer();
  }

  onAddNewSubmit = (values) => {
    const { suppliers } = this.state;
    suppliers.push({ ...values });
    this.updateDB(suppliers);
    this.hideDrawer();
  }

  onSearch = () => {
    console.log('onSearch');
  }

  render() {
    const { mode } = this.state;
    return (
      <div>
        <SearchSupplier onSearch={this.onSearch} />
        <Button type="primary" onClick={this.onNew}>Add new</Button>
        {this.state.showDrawer && <SupplierForm
          title={(mode === EDIT) ? 'Edit supplier' : 'New supplier'}
          onClose={this.hideDrawer}
          visible={this.state.showDrawer}
          submitBtnText={(mode === EDIT) ? 'Save' : 'Save and Add'}
          onSubmit={this.onSubmit}
          initialValues={this.state.currentSupplier || {}}
        />}
        <SuppliersList
          suppliers={this.state.suppliers}
          onEdit={this.onEdit}
        />
      </div>
    );
  }
}

export default WrapperHoc(Suppliers);
