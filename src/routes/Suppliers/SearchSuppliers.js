import React, {Component} from 'react';
import SearchBox from '../../components/SearchBox';

export default class SearchSupplier extends Component {
  render() {
    const formItems = [
      {
        label: 'Supplier Name',
        key: 'supplierName',
        isRequired: true,
        placeholder: 'Enter Supplier Name',
        type: 'text',
        inputProps: {
          width: 450,
        },
      },
      {
        label: 'Mobile No.',
        key: 'mobileNumber',
        isRequired: true,
        type: 'text',
        inputProps: { width: 450 },
        validations: [
          {
            pattern: /^[6789]\d{9}$/,
            message: 'Enter valid mobile number',
          },
        ],
      },
      {
        label: 'Contact Person',
        key: 'contactPerson',
        isRequired: true,
        placeholder: 'Contanct Person',
        type: 'text',
        inputProps: { width: 450 },
      },
    ];
    return (
      <SearchBox
        onSearch={this.props.onSearch}
        formItems={formItems}
      />
    );
  }
}
