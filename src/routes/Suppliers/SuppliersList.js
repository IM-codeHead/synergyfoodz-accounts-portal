import React, {Component} from 'react';
import { Table, Button } from 'antd';
import { STATES } from '../../constants';

export default class SuppliersList extends Component {
  render() {
    const columns = [
      {
        title: 'Action',
        dataIndex: 'supplierName',
        key: 'editButton',
        render: (supplierName) => <Button icon="edit" onClick={() => this.props.onEdit(supplierName)} />
      },
      {
        title: 'Supplier Name',
        dataIndex: 'supplierName',
        key: 'supplierName',
      },
      {
        title: 'State',
        dataIndex: 'stateCode',
        key: 'stateCode',
        render: (stateCode) => <div>{STATES.find(el => el.value === stateCode).label}</div>
      },
      {
        title: 'Contact person',
        dataIndex: 'contactPerson',
        key: 'contactPerson',
      },
      {
        title: 'Mobile number',
        dataIndex: 'mobileNumber',
        key: 'mobileNumber',
      },
    ];

    return (
      <div className="list supplier-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
          dataSource={this.props.suppliers}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['supplierName']}
        />
      </div>
    );
  }
}
