import React, {Component} from 'react';
import { Drawer } from 'antd';
import Form from '../../components/Form';
import { STATES } from '../../constants';

export default class SupplierForm extends Component {
  render() {
    const { initialValues } = this.props;
    const formItems = [
      {
        label: 'Supplier Name',
        key: 'supplierName',
        isRequired: true,
        placeholder: 'Enter Supplier Name',
        type: 'text',
        inputProps: {
          width: 450,
        },
        value: initialValues.supplierName,
      },
      {
        label: 'Supplier Address',
        key: 'supplierAddress',
        isRequired: true,
        placeholder: 'Enter Address',
        type: 'textArea',
        inputProps: { rows: 2, width: 450 },
        value: initialValues.supplierAddress,
      },
      {
        label: 'State',
        key: 'stateCode',
        isRequired: true,
        placeholder: '',
        type: 'select',
        inputProps: {
          width: 450,
          options: STATES,
        },
        value: initialValues.stateCode,
      },
      {
        label: 'GSTIN',
        key: 'GSTIN',
        isRequired: true,
        placeholder: 'GSTIN',
        type: 'text',
        inputProps: { width: 450 },
        value: initialValues.GSTIN,
      },
      {
        label: 'PAN NO.',
        key: 'panNumber',
        isRequired: true,
        placeholder: 'PAN NO.',
        type: 'text',
        inputProps: { width: 450 },
        value: initialValues.panNumber,
      },
      {
        label: 'Mobile No.',
        key: 'mobileNumber',
        isRequired: true,
        type: 'text',
        inputProps: { width: 450 },
        validations: [
          {
            pattern: /^[6789]\d{9}$/,
            message: 'Enter valid mobile number',
          },
        ],
        value: initialValues.mobileNumber,
      },
      {
        label: 'Contact Person',
        key: 'contactPerson',
        isRequired: true,
        placeholder: 'Contanct Person',
        type: 'text',
        inputProps: { width: 450 },
        value: initialValues.contactPerson,
      },
    ];

    return (
      <Drawer
        title={this.props.drawerTitle}
        width={'100%'}
        onClose={this.props.onClose}
        visible={this.props.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          submitBtnText={this.props.submitBtnText}
          onSubmit={this.props.onSubmit}
          formItems={formItems}
        />
      </Drawer>
    );
  }
}
