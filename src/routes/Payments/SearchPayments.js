import React, {Component} from 'react';
import SearchBox from '../../components/SearchBox';
import { database } from '../../firebase';

export default class SearchPayments extends Component {
  state = { suppliers: [] };

  componentDidMount() {
    database.ref('/suppliers').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ suppliers: state.map(el => el.supplierName) });
    });
  }

  render() {
    const suppliers = this.state.suppliers.map(supplier => { return { value: supplier, label: supplier }; });
    const formItems = [
      {
        label: 'Payment Date',
        key: 'paymentDate',
        isRequired: true,
        placeholder: 'Enter payment date',
        type: 'date',
        inputProps: {
          width: 450,
        },
      },
      {
        label: 'Supplier Name',
        key: 'supplierName',
        isRequired: true,
        inputProps: {
          width: 320,
          options: suppliers,
        },
        type: 'select',
      },
      {
        label: 'Payment amount',
        key: 'paymentAmount',
        isRequired: true,
        placeholder: '',
        type: 'number',
        inputProps: { width: 250 },
      },
    ];
    return (
      <SearchBox
        onSearch={this.props.onSearch}
        formItems={formItems}
      />
    );
  }
}
