import React, {Component} from 'react';
import { Drawer } from 'antd';
import moment from 'moment';
import Form from '../../components/Form';
import { database } from '../../firebase';
import BillsList from './BillsList';

export default class PaymentForm extends Component {
  state = { suppliers: [], paymentMode: this.props.initialValues.paymentMode };

  componentDidMount() {
    database.ref('/suppliers').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ suppliers: state.map(el => el.supplierName) });
    });
  }

  onPaidAmountChange = (e, record) => {
    console.log(e, record);
  }

  onPaymentModeChange = e => {
    this.setState({ paymentMode: e.target.value });
  }

  render() {
    const { initialValues } = this.props;
    const suppliers = this.state.suppliers.map(supplier => { return { value: supplier, label: supplier }; });
    let purchaseBills = [];
    if (initialValues.supplierName) {
      purchaseBills = this.props.purchaseBills.filter(el => el.supplierName === initialValues.supplierName);
    }

    let chequeFormItems = [];
    if (this.state.paymentMode === 'cheque') {
      chequeFormItems = [
        {
          label: 'Cheque Number',
          key: 'chequeNumber',
          isRequired: true,
          placeholder: '',
          type: 'number',
          inputProps: {
            width: 350,
          },
          value: initialValues.chequeNumber,
        },
        {
          label: 'Cheque Date',
          key: 'chequeDate',
          isRequired: true,
          placeholder: '',
          type: 'date',
          inputProps: {
            width: 350,
          },
          value: initialValues.chequeDate ? moment(initialValues.chequeDate, 'DD/MM/YYYY') : null,
        },
      ];
    }

    const formItems = [
      {
        label: 'Payment Date',
        key: 'paymentDate',
        isRequired: true,
        placeholder: '',
        type: 'date',
        value: initialValues.paymentDate ? moment(initialValues.paymentDate, 'DD/MM/YYYY') : null,
      },
      {
        label: 'Supplier Name',
        key: 'supplierName',
        isRequired: true,
        inputProps: {
          width: 320,
          options: suppliers,
        },
        type: 'select',
        value: initialValues.supplierName,
      },
      {
        label: 'Payment Amount',
        key: 'paymentAmount',
        isRequired: true,
        placeholder: '',
        type: 'number',
        inputProps: {
          width: 350,
        },
        value: initialValues.paymentAmount,
      },
      {
        label: 'Payment Mode',
        key: 'paymentMode',
        isRequired: true,
        placeholder: '',
        type: 'radio',
        inputProps: {
          options: [{ label: 'Cheque', value: 'cheque' }, { label: 'Cash', value: 'cash' }, { label: 'RTGS', value: 'RTGS' }],
          onChange: this.onPaymentModeChange,
        },
        value: initialValues.paymentMode,
      },
      ...chequeFormItems,
      {
        label: 'Remarks',
        key: 'remarks',
        isRequired: false,
        placeholder: '',
        type: 'textArea',
        inputProps: { rows: 2, width: 350 },
        value: initialValues.remarks,
      },
    ];

    return (
      <Drawer
        title={this.props.drawerTitle}
        width={'100%'}
        onClose={this.props.onClose}
        visible={this.props.visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          submitBtnText={this.props.submitBtnText}
          onSubmit={this.props.onSubmit}
          formItems={formItems}
        />
        <BillsList purchaseBills={purchaseBills} onPaidAmountChange={this.onPaidAmountChange} />
      </Drawer>
    );
  }
}
