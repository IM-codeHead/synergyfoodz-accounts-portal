import React, {Component} from 'react';
import { Table, Button } from 'antd';

export default class PaymentsList extends Component {
  render() {
    const columns = [
      {
        title: 'Action',
        dataIndex: 'paymentId',
        key: 'editButton',
        render: (paymentId) => <Button icon="edit" onClick={() => this.props.onEdit(paymentId)} />
      },
      {
        title: 'Payment Id',
        dataIndex: 'paymentId',
        key: 'paymentId',
      },
      {
        title: 'Payment Date',
        dataIndex: 'paymentDate',
        key: 'paymentDate',
      },
      {
        title: 'Supplier',
        dataIndex: 'supplierName',
        key: 'supplierName',
      },
      {
        title: 'Amount',
        dataIndex: 'paymentAmount',
        key: 'paymentAmount',
      },
    ];

    return (
      <div className="list payments-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
          dataSource={this.props.payments}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['paymentId']}
        />
      </div>
    );
  }
}
