import React, {Component} from 'react';
import WrapperHoc from '../../components/WrapperHoc';
import { Button, Select } from 'antd';
import PaymentForm from './PaymentForm';
import PaymentsList from './PaymentsList';
import SearchPayments from './SearchPayments';
import { database } from '../../firebase';
import './index.scss';

const { Option } = Select;

const EDIT = 'edit';
const NEW = 'new';

class Payments extends Component {
  state = { showDrawer: false, mode: null, locations: [], currentLocation: null, payments: [], currentPayment: null, purchaseBills: [] };
  payments = [];

  componentDidMount() {
    database.ref('/locations').on('value', snapshot => {
      const state = snapshot.val();
      this.setState({ locations: state.map(el => el.locationId) });
    });
  }

  updateDB(payments) {
    const rootRef = database.ref();
    const storesRef = rootRef.child('/payments');
    storesRef.set(payments);
  }

  onNew = () => {
    this.setState({ showDrawer: true, mode: NEW });
  }

  onEdit = (paymentId) => {
    const currentPayment = this.state.payments.find(el => el.paymentId === paymentId);
    this.setState({ showDrawer: true, mode: EDIT, currentPayment });
  }

  hideDrawer = () => {
    this.setState({ showDrawer: false, mode: null });
  }

  onSubmit = (form) => {
    const {validateFields, getFieldsValue} = form;
    validateFields((errors) => {
      if (errors) {
        return errors;
      }
      const fieldValues = getFieldsValue();
      if (this.state.mode === EDIT) {
        this.onEditSubmit(fieldValues);
      } else if (this.state.mode === NEW) {
        this.onAddNewSubmit(fieldValues);
      }
    });
  }

  onEditSubmit = (values) => {
    const { payments, currentPayment } = this.state;
    const currentPaymentIndex = payments.findIndex(el => el.paymentId === currentPayment.paymentId);
    payments[currentPaymentIndex] = { ...values };
    this.updateDB(payments);
    this.setState({ currentPayment: null });
    this.hideDrawer();
  }

  onAddNewSubmit = (values) => {
    const { payments } = this.state;
    payments.push({ ...values, paymentId: `payment-${payments.length}` });
    this.updateDB(payments);
    this.hideDrawer();
  }

  onSearch = () => {
    console.log('onSearch');
  }

  onLocationChange = value => {
    this.setState({ currentLocation: value }, () => {
      database.ref(`/payments/${this.state.currentLocation}`).on('value', snapshot => {
        const state = snapshot.val();
        this.setState({ payments: state });
        this.payments = state;
      });
      database.ref(`/purchaseBills/${this.state.currentLocation}`).on('value', snapshot => {
        const state = snapshot.val();
        this.setState({ purchaseBills: state });
      });
    });
  }

  render() {
    const { mode } = this.state;
    return (
      <div>
        <div className="select-location">
          <div style={{ marginRight: 10 }}><strong>Select Location </strong></div>
          <Select style={{ width: 120 }} onChange={this.onLocationChange}>
            {this.state.locations.map(locationId => <Option key={locationId} value={locationId}>{locationId}</Option>)}
          </Select>
        </div>
        {this.state.currentLocation && (
          <div className="location-bill-card flex-center-center flex-column">
            <div className="title">{this.state.currentLocation}</div>
            <SearchPayments onSearch={this.onSearch} />
            <Button type="primary" onClick={this.onNew}>Add new payment</Button>
            {this.state.showDrawer && <PaymentForm
              title={(mode === EDIT) ? 'Edit payment' : 'New payment'}
              onClose={this.hideDrawer}
              visible={this.state.showDrawer}
              submitBtnText={(mode === EDIT) ? 'Save' : 'Save and Add'}
              onSubmit={this.onSubmit}
              initialValues={this.state.currentPayment || {}}
              purchaseBills={this.state.purchaseBills}
            />}
            <PaymentsList
              payments={this.state.payments}
              onEdit={this.onEdit}
            />
          </div>
        )}
        {!this.state.currentLocation && <div>Please select location from the above list</div>}
      </div>
    );
  }
}

export default WrapperHoc(Payments);
