import React, {Component} from 'react';
import { Table, InputNumber } from 'antd';

export default class BillsList extends Component {
  render() {
    const purchaseBills = this.props.purchaseBills.map(bill => {
      return {
        ...bill,
        balanceAmount: bill.grossBillAmount - bill.paidAmount,
      };
    })
    const columns = [
      // {
      //   title: 'Action',
      //   dataIndex: 'paymentId',
      //   key: 'editButton',
      //   render: (paymentId) => <Button icon="edit" onClick={() => this.props.onEdit(paymentId)} />
      // },
      {
        title: 'Bill No.',
        dataIndex: 'billNumber',
        key: 'billNumber',
      },
      {
        title: 'Bill Date',
        dataIndex: 'receiptDate',
        key: 'receiptDate',
      },
      {
        title: 'Balance Amount',
        dataIndex: 'balanceAmount',
        key: 'balanceAmount',
      },
      {
        title: 'Paid Amount',
        dataIndex: 'paidAmount',
        key: 'paidAmount',
        render: (paidAmount, record) => <InputNumber value={paidAmount} style={{ width: 80 }} onChange={(e) => this.props.onPaidAmountChange(e, record)} />
      },
      {
        title: 'Total Amount',
        dataIndex: 'grossBillAmount',
        key: 'grossBillAmount',
      },
    ];

    return (
      <div className="list payments-list flex-center-center flex-column">
        <Table
          columns={columns}
          bordered
          dataSource={purchaseBills}
          rowClassName={(record) => {
            return record.isPending ? 'table-row-color' : '';
          }}
          rowKey={record => record['billNumber']}
        />
      </div>
    );
  }
}
