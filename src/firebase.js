import * as firebase from 'firebase';

let config = {
 apiKey: "AIzaSyACjSP4H38v_qL-r4SU6TXxGi9teU1leUk",
 authDomain: "foodbillweb.firebaseapp.com",
 databaseURL: "https://foodbillweb.firebaseio.com",
 projectId: "foodbillweb",
 storageBucket: "foodbillweb.appspot.com",
 messagingSenderId: "422684335713",
 appId: "1:422684335713:web:a1b7f2e8a04de833"
}

firebase.initializeApp(config);
export const database = firebase.database();
export const storage = firebase.storage();
